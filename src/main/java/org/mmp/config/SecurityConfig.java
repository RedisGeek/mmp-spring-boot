package org.mmp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.web.access.AccessDeniedHandler;

public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private AccessDeniedHandler accesDeniedHandler;
	
	@Override
	public void configure(HttpSecurity http) throws Exception{
		
		http.csrf().disable()
					.authorizeRequests()
						.antMatchers("/","/home","/about").permitAll()
						.antMatchers("/admin/**").hasAnyRole("ADMIN")
						.antMatchers("/users/**").hasAnyRole("ROLE")
						.anyRequest().authenticated()
					.and()
					.formLogin()
						.loginPage("/login")
						.permitAll()
						.and()
					.logout()
						.permitAll()
						.and()
					.exceptionHandling()
						.accessDeniedHandler(accesDeniedHandler);
						
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
		auth.inMemoryAuthentication()
			.withUser("user").password("password").roles("USERS")
			.and()
			.withUser("admin").password("password").roles("ADMIN");
	}
	
}
