package org.mmp.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmpWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmpWebAppApplication.class, args);
	}
}
